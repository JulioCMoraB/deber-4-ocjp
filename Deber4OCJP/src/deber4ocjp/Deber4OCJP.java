/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deber4ocjp;

/**
 *
 * @author julio_000
 */
public class Deber4OCJP {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        //Instanciacion de las clases
        Object objeto= new Object();
        System.out.println(objeto.toString());
        Fruit fruta=new Fruit();
        System.out.println(fruta.toString());
        Apple manzana=new Apple();
        System.out.println(manzana.toString());
        Citrus citrico=new Citrus();
        System.out.println(citrico.toString());
        Orange naranja= new Orange();
        System.out.println(naranja.toString());
        Squeezable squeezable= new Squeezable() {};
        System.out.println(squeezable.toString());
        //Casting e implementacion de excepciones si el casting falla
        try {
            objeto=(Fruit)fruta;
            System.out.println(objeto.toString());
        } catch (Exception e) {
            System.err.println("No se puede hacer casting");
        }
         try {
            fruta=(Apple)manzana;
           System.out.println(fruta.toString());
        } catch (Exception e) {
            System.err.println("No se puede hacer casting");
        }
          try {
            manzana=(Apple)(Squeezable)squeezable;
           System.out.println(manzana.toString());
        } catch (Exception e) {
            System.err.println("No se puede hacer casting");
        }
         try {
            citrico=(Citrus)citrico;
            System.out.println(citrico.toString());
        } catch (Exception e) {
            System.err.println("No se puede hacer casting");
        }
          try {
            naranja=(Orange)naranja;
           System.out.println(naranja.toString());
        } catch (Exception e) {
            System.err.println("No se puede hacer casting");
        }
    }
    
}
